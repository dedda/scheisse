/**
 * Package containing {@link org.dedda.games.scheisse.state.game.quest.Quest}s
 * and all the quest related logic.
 */
package org.dedda.games.scheisse.state.game.quest;
