/**
 * Package containing {@link org.dedda.games.scheisse.state.game.map.soil.Soil}
 * classes for the {@link org.dedda.games.scheisse.state.game.map.Map}.
 */
package org.dedda.games.scheisse.state.game.map.soil;
