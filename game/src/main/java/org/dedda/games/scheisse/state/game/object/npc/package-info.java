/**
 * Package containing
 * {@link org.dedda.games.scheisse.state.game.object.npc.NPC}s
 * that occur in the game.
 */
package org.dedda.games.scheisse.state.game.object.npc;
