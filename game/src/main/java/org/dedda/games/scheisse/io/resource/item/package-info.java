/**
 * Package containing classes used for loading
 * {@link org.dedda.games.scheisse.state.game.item.Item}s that are used
 * in the {@link org.dedda.games.scheisse.state.game.Game}
 */
package org.dedda.games.scheisse.io.resource.item;
