/**
 * Package containing all existing
 * {@link org.dedda.games.scheisse.state.game.map.level.Level}s on the
 * {@link org.dedda.games.scheisse.state.game.map.Map} and the logic
 * that is needed to connect them and make them work properly.
 */
package org.dedda.games.scheisse.state.game.map.level;
