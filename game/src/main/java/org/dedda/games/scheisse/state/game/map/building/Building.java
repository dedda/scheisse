package org.dedda.games.scheisse.state.game.map.building;

import org.dedda.games.scheisse.state.game.map.MapObject;

import java.awt.Dimension;
import java.awt.Point;

/**
 * Created by dedda on 9/15/14.
 */
public class Building extends MapObject {

    public Building(final Point location, final Dimension size) {
        super(location, size);
    }

}
