/**
 * Package containing all classes needed for creating a
 * {@link org.dedda.games.scheisse.state.game.map.Map} for the
 * {@link org.dedda.games.scheisse.state.game.Game}.
 */
package org.dedda.games.scheisse.state.game.map;
