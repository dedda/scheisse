package org.dedda.games.scheisse.exception.io.world;

/**
 * {@link java.lang.Exception} which is thrown when trying to load a world from
 * a directory that doesn't contain any or incomplete world data.
 */
public class NoWorldDirectoryException extends Exception {

}
