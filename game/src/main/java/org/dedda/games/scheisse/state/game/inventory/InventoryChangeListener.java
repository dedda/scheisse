package org.dedda.games.scheisse.state.game.inventory;

/**
 * Created by dedda on 10/6/14.
 */
public interface InventoryChangeListener {

    public abstract void inventoryChangeAction();

}
