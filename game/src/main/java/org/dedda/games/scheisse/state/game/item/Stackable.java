package org.dedda.games.scheisse.state.game.item;

/**
 * Created by dedda on 4/18/14.
 */
public interface Stackable {

    /**
     * @return int
     */
    public abstract int maxStackNumber();

}
