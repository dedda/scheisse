/**
 * Package containing everything building related on the
 * {@link org.dedda.games.scheisse.state.game.map.Map}.
 */
package org.dedda.games.scheisse.state.game.map.building;
