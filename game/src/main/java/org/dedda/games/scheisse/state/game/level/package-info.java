/**
 * Package containing game
 * {@link org.dedda.games.scheisse.state.game.map.level.Level}s and
 * level logic.
 */
package org.dedda.games.scheisse.state.game.level;
